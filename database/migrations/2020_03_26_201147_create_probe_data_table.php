<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProbeDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('probe_data', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->double('temperature');
            $table->integer('humidity');

            $table->unsignedBigInteger('probe_id')->nullable();
            $table->foreign('probe_id')->references('id')->on('probes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('probe_data');
    }
}
