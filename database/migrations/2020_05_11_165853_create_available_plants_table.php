<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailablePlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_plants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('alternate_name');
            $table->text('sow_instructions');
            $table->text('space_instructions');
            $table->text('harvest_instructions');
            $table->text('compatible_plants');
            $table->text('avoid_instructions');
            $table->text('culinary_hints');
            $table->text('culinary_preservation');
            $table->string('url');
            $table->double('temperature_min');
            $table->double('temperature_max');
            $table->double('humidite_min');
            $table->double('humidite_max');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_plants');
    }
}
