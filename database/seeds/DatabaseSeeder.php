<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create admin user
        DB::table('users')->insert([
            'name' => 'Administrateur',
            'email' => 'admin@gardom.fr',
            'password' => Hash::make('root'),
            'admin' => 1,
            'city' => 'Lyon',
            'active' => 1
        ]);

        $this->call(AvailablePlantsSeeder::class);
    }
}
