<?php

use Illuminate\Database\Seeder;

class AvailablePlantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plants = [
            ['Amarante', 'Love-lies-saignement', 'Semez dans le jardin. Semez les graines à une profondeur d environ trois fois le diamètre de la graine. Mieux plantées à des températures du sol entre 64 ° F et 86 ° F. (Afficher ° C / cm) ', ' Plantes spatiales espacées de 20 pouces ', ' Récolte en 7 à 8 semaines. ', ' Compatible avec (peut pousser à côté): Oignons , maïs , poivrons , aubergine , tomates ', ' ', ' Les feuilles et les graines peuvent être utilisées. Un apport excessif n est pas recommandé. ', ' ', ' Http: //gardenate.com/plant/Amaranth ', '64', '86', '70', '85'],
            ['Angelica', '', 'Facile à cultiver. Semer dans le jardin. Semer les graines à une profondeur d environ trois fois le diamètre de la graine. Mieux planté à des températures du sol entre 50 ° F et 77 ° F. (Afficher ° C / cm) ', ' Plantes spatiales espacées de 18 pouces ', ' Récolter dans environ 18 mois. Angelica archangelica a des feuilles légèrement ternes , non brillantes .. ', ' Compatible avec (peut pousser à côté): Toutes les herbes qui aiment l humidité , zones ombragées - menthe , mélisse ', ' ', ' Les tiges peuvent être confites et utilisées pour décorer des gâteaux et des pâtisseries. Choisissez les tiges la deuxième année. ', ' ', ' http: //gardenate.com/plant /Angélique', '50', '77', '72', '82'],
            ['Artichauts (Globe)', '', 'Facile à cultiver. Semer dans le jardin. Semer les graines à une profondeur d environ trois fois le diamètre de la graine. Mieux plantées à des températures du sol entre 59 ° F et 64 ° F. (Afficher ° C / cm) ', ' Plantes spatiales: espacées de 63 à 79 pouces ', ' Récolte en 42-57 semaines. ', ' Compatible avec (peut pousser à côté): Nécessite beaucoup d espace. Meilleur en lit séparé ', ' ', ' Cueillez les bourgeons avant que les écailles ne développent des pointes brunes. Si vous avez beaucoup de petits bourgeons , ils peuvent être frits dans l huile d olive et mangés entiers. Rincer abondamment à l eau froide pour éliminer les perce-oreilles ou autres insectes. ', ' ', ' http://gardenate.com/plant/Artichokes%2B(Globe) ', '59', '64', '73', '83'],
        ];

        $plantsToInsert = [];
        foreach ($plants as $plant){
            array_push($plantsToInsert, [
                'name' => $plant[0],
                'alternate_name' => $plant[1],
                'sow_instructions' => $plant[2],
                'space_instructions' => $plant[3],
                'harvest_instructions' => $plant[4],
                'compatible_plants' => $plant[5],
                'avoid_instructions' => $plant[6],
                'culinary_hints' => $plant[7],
                'culinary_preservation' => $plant[8],
                'url' => $plant[9],
                'temperature_min' => $plant[10],
                'temperature_max' => $plant[11],
                'humidite_min' => $plant[12],
                'humidite_max' => $plant[13],
            ]);
        }

        DB::table('available_plants')->insert($plantsToInsert);
    }
}
