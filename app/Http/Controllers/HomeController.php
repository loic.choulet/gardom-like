<?php

namespace App\Http\Controllers;

use App\Probe;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $probes = Probe::all();
        $weather = $this->getWeather();

        debug($weather);

        return view('home', compact('probes', 'weather'));
    }

    private function getWeather()
    {
        // Récupère la ville de l'utilisateur connecté
        $city = Auth::user()->city;

        // Si la météo pour cette ville est présente dans le cache, récupère la météo depuis le cache
        // Le cache expire toute les heures
        if (Cache::has('weather-'.$city)) {
            return Cache::get('weather-'.$city);
        }
        // Si la météo de la ville demandé n'existe pas en cache, requête vers openweather
        // Insertion de la réponse dans le cache pour 1h
        else {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => 'https://api.openweathermap.org/',
                // You can set any number of default request options.
                'timeout' => 10.0,
            ]);

            try {
                $response = $client->request('GET', 'data/2.5/weather', [
                    'query' => [
                        'q' => Auth::user()->city,
                        'appid' => env('OPENWEATHER_KEY'),
                        'lang' => 'fr',
                        'units' => 'metric',
                    ]
                ]);

                Log::info('Request to open weather');
                $value = json_decode($response->getBody()->getContents());
                Cache::put('weather-'.$city, $value, 3600);
                return $value;
            } catch (\Exception $e) {
                Log::error($e);
                return false;
            }
        }
    }
}
