<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Probe;
use Illuminate\Http\Request;

class ProbesController extends Controller
{
    public function getData(Request $request, Probe $probe){
        $data = $probe->data()
            ->whereDate('created_at', '>=', $request->query('startDate'))
            ->whereDate('created_at', '<=', $request->query('endDate'))
            ->orderBy('created_at')
            ->get();

        // Treat data

        $labels = [];
        $temperature = [];
        $humidity = [];

        // Si la date séléctionné = 1 jour
        if(date('Ymd', strtotime ($request->query('startDate'))) == date('Ymd', strtotime($request->query('endDate')))){
            foreach ($data as $row){
                array_push($labels, $row->created_at->format('H:i'));
                array_push($temperature, $row->temperature);
                array_push($humidity, $row->humidity);
            }
        }
        // Si la date séléctionné > 1 jour
        else{
            $grouped = $data->groupBy(function ($item, $key) {
                return $item->created_at->format('d-m-Y');
            });

            foreach ($grouped as $key => $date){
                array_push($labels, $key);
                array_push($temperature, round($date->avg('temperature'), 2));
                array_push($humidity, round($date->avg('humidity'), 2));
            }
        }

        $result = [
            'labels' => $labels,
            'temperature' => $temperature,
            'humidity' => $humidity,
        ];

        return response($result);
    }
}
