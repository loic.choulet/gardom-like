<?php

namespace App\Http\Controllers;

use App\Mail\FinishAccount;
use Illuminate\Http\Request;
use App\User;
use App\Probe;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'admin' => ['required', 'boolean'],
            'city' => ['required', 'string', 'max:255'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'admin' => $request->admin,
            'city' => $request->city
        ]);

        Mail::to($user->email)->send(new FinishAccount($user));

        return redirect(route('users.index'))->with('success', 'Utilisateur créé avec succès');
    }

    public function finish(User $user){
        if (!$user->password == null){
            return redirect('/')->with('error', "Ce compte est déjà finalisé");
        }

        return view('users.validate', compact('user'));
    }

    public function storeFinish(Request $request, User $user){
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user->password = Hash::make($request->password);
        $user->active = 1;
        $user->save();

        // Connexion automatique de l'utilisateur
        Auth::guard()->login($user);

        // Redirection sur la page d'accueil
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
