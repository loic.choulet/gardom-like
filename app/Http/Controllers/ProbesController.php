<?php

namespace App\Http\Controllers;

use App\Probe;
use App\ProbeData;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProbesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $probes = Probe::all();
        return view('probes.index', compact('probes'));
    }

    public function store(Request $request)
    {
        $probe = Probe::create([
            'name' => $request->name,
            'type' => $request->type,
        ]);

        if ($request->has('add-test-data')) {
            $dateStart = Carbon::now()->hour(0)->subMonths(1);
            $dateEnd = Carbon::now();

            while ($dateStart->lessThan($dateEnd)) {
                ProbeData::create([
                    'probe_id' => $probe->id,
                    'temperature' => rand(-10, 45),
                    'humidity' => rand(0, 100),
                    'created_at' => $dateStart
                ]);
                $dateStart->addHour();
            }
        }

        return redirect(route('probes.index'))->with('success', "Capteur créé avec succès !");
    }

    /**
     * @param Probe $probe
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Probe $probe)
    {
        return view('probes.view', compact('probe'));
    }
}
