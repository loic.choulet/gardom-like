<?php

namespace App\Http\Controllers;

use App\AvailablePlant;
use App\Plant;
use Illuminate\Http\Request;

class PlantsController extends Controller
{
    public function index(){
        $plants = Plant::all();

        $exclude = [];
        foreach ($plants as $plant){
            array_push($exclude, $plant->available_plant_id);
        }

        $availablePlants = AvailablePlant::whereNotIn('id', $exclude)->get();
        return view('plants.index', compact('plants', 'availablePlants'));
    }

    public function store(Request $request){
        Plant::create(['available_plant_id' => $request->input('plant-to-add')]);
        return redirect(route('plants.index'))->with('success', "Plante ajoutée avec succès");
    }

    public function show(Plant $plant){
        return view('plants.show', compact('plant'));
    }

    public function destroy(Plant $plant){
        $plant->delete();
        return redirect(route('plants.index'))->with('success', "Plante supprimée avec succès");
    }
}
