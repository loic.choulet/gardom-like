<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Probe extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type'
    ];


    /**
     * Get probes of user.
     */
    public function data()
    {
        return $this->hasMany('App\ProbeData')->orderBy('created_at');
    }
}
