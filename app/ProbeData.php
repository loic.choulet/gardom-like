<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProbeData extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'probe_id', 'temperature', 'humidity', 'created_at'
    ];
}
