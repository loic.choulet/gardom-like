<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $fillable = ['available_plant_id'];

    public function availablePlant(){
        return $this->belongsTo('App\AvailablePlant');
    }
}
