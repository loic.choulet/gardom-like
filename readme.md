
# Gardom Like

## Development

### Install project
```
git clone https://gitlab.com/loic.choulet/gardom-like.git
cd gardom-like
composer install
cp .env.example .env
```

### Configure project
In .env file, configure APP_URL, database, smtp server & open weather API key

### Create DB structure & data

```
php artisan key:generate
php artisan migrate --seed
```

You can access to gardom like project !

### Root account

```
email : admin@gardom.fr
password : root
```
