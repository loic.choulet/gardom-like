@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{$plant->availablePlant->name}}</h3>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Sow instructions</h3>
        </div>
        <div class="card-header">
            <p>{{$plant->availablePlant->sow_instructions}}</p>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Space instructions</h3>
        </div>
        <div class="card-header">
            <p>{{$plant->availablePlant->space_instructions}}</p>
        </div>
    </div>

@endsection
