@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Mes plantes</h3>

            <div class="card-tools">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                        data-target="#modal-create-probe">
                    Ajouter une plante
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Nom</th>
                                <th>Nom alternatif</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($plants as $plant)

                                <tr>
                                    <td>{{$plant->id}}</td>
                                    <td>{{$plant->availablePlant->name}}</td>
                                    <td>{{$plant->availablePlant->alternate_name}}</td>
                                    <td>
                                        <a href="{{route('plants.show', $plant)}}" class="btn btn-default"><i class="fas fa-eye"></i></a>

                                        <a href="#" onclick="event.preventDefault();document.getElementById('destroy-form-{{$plant->id}}').submit();" class="btn btn-default"><i class="fas fa-trash"></i></a>

                                        <form action="{{route('plants.destroy', $plant)}}" method="post" id="destroy-form-{{$plant->id}}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
        </div>
        <!-- /.card-body -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-create-probe" tabindex="-1" role="dialog" aria-labelledby="modal-create-probe">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ajouter une plante</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('plants.store')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="plant-to-add">Séléctionner la plante à ajouter</label>
                            <select id="plant-to-add" name="plant-to-add" class="form-control">
                                @foreach($availablePlants as $plant)
                                    <option value="{{$plant->id}}">{{$plant->name}} - {{$plant->alternate_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
