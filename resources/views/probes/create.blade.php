@extends('layouts.app')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Créer un capteur</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{ route('probes.store') }}">
            @csrf

            <div class="card-body">

                <div class="form-group">
                    <label for="email">Name</label>
                    <input type="text" name="name" id="name"
                           class="form-control @error('name') is-invalid @enderror" placeholder="Enter name"
                           value="{{ old('name') }}" required>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" id="email"
                           class="form-control @error('email') is-invalid @enderror" placeholder="Enter email"
                           value="{{ old('email') }}" required>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                {{-- TODO : gestion des erreurs --}}
                <div class="form-group">
                    <label>Rôle</label>
                    <select class="form-control" name="admin" id="admin">
                        <option value="0">Utilisateur</option>
                        <option value="1">Administrateur</option>
                    </select>
                </div>

                <label>Liste des capteurs non attribués</label>

                @foreach($probesUnassigned as $probe)

                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="probes{{$probe->id}}" name='probes[]' value='{{$probe->id}}'>
                        <label class="form-check-label" for="probes{{$probe->id}}">Capteur #{{$probe->id}}</label>
                    </div>

                @endforeach

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>

@endsection
