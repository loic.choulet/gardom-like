@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-12">
            <chart-component title="{{$probe->name}}" id="{{$probe->id}}"
                             keepalive="{{$probe->keepalive}}"></chart-component>
        </div>

    </div>
@endsection
