@extends('layouts.app')

@section('content')
{{--    TODO : modal only for admin--}}

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Capteurs</h3>

            <div class="card-tools">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                        data-target="#modal-create-probe">
                    Nouveau capteur
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
            <table class="table">
                <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Nom</th>
                    <th>Type</th>
                    <th>Connecté</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($probes as $probe)

                    <tr>
                        <td>{{$probe->id}}</td>
                        <td>{{$probe->name}}</td>
                        <td>{{$probe->type}}</td>
                        <td>{{$probe->keepalive ? 'Oui' : 'Non'}}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{route('probes.show', $probe)}}" class="btn btn-default"><i class="fas fa-eye"></i></a>
                            </div>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-create-probe" tabindex="-1" role="dialog" aria-labelledby="modal-create-probe">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Créer un capteur</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('probes.store')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Entrer un nom">
                        </div>

                        <div class="form-group">
                            <label for="type">Type</label>
                            <input type="text" class="form-control" name="type" id="type" placeholder="Entrer un type">
                        </div>

                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="add-test-data" id="add-test-data">
                            <label class="form-check-label" for="add-test-data">Insérer des données de test ?</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Créer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
