<p>Bonjour {{$user->name}} !</p>

<p>Pour finaliser votre compte, il vous suffit de cliquer sur le lien suivant et de configurer votre mot de passe</p>

<a href="{{route('users.finish', $user)}}">Finaliser mon compte</a>

<p>A bientôt sur {{config('app.name')}} !</p>
