@extends('layouts.login')

@section('content')
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">{{ __('Création du mot de passe') }}</p>

            <form method="POST" action="{{ route('users.finish', $user) }}">
                @csrf

                <div class="input-group mb-3">
                    <input id="name" type="text" class="form-control" name="name" disabled value="{{$user->name}}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                           placeholder="{{ __('Password') }}" name="password" required autocomplete="current-password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror

                <div class="input-group mb-3">
                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                           placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required autocomplete="new-password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror

                <div class="row">
                    <div class="col-5">

                    </div>
                    <!-- /.col -->
                    <div class="col-7">
                        <button type="submit" class="btn btn-primary btn-block">Finaliser l'inscription</button>
                    </div>
                    <!-- /.col -->
                </div>

            </form>
        </div>
    </div>

@endsection
