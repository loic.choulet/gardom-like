@extends('layouts.app')

@section('content')

    <div class="row">
        @foreach($probes as $probe)
            <div class="col-sm-6 col-12">
                <chart-component title="{{$probe->name}}" id="{{$probe->id}}" keepalive="{{$probe->keepalive}}"></chart-component>
            </div>
        @endforeach

        <div class="col-md-3 col-sm-6 col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Météo à {{$weather->name}}</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="info-box bg-warning">
                        <span class="info-box-icon"><i class="fas fa-thermometer-half"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Temps {{$weather->weather[0]->description}}</span>
                            <span class="info-box-number">{{round($weather->main->temp)}}°C</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                          Min : {{round($weather->main->temp_min)}}°C | Max : {{round($weather->main->temp_max)}}°C
                </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
