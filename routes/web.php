<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// TODO : user not connected
Route::post('/users/{user}/finish', 'UsersController@storeFinish')->name('users.finish');
Route::get('/users/{user}/finish', 'UsersController@finish');

/* User connected */
Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index');

    Route::resource('probes', 'ProbesController')->only(['index', 'show']);
    Route::resource('plants', 'PlantsController')->only(['index', 'show']);
});

/* Admin */
Route::middleware(['auth', 'admin'])->group(function () {
    Route::resource('users', 'UsersController')->only(['index', 'store']);
    Route::resource('probes', 'ProbesController')->only(['store']);
    Route::resource('plants', 'PlantsController')->only(['store', 'destroy']);
});

